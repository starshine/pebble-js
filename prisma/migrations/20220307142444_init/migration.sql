-- CreateTable
CREATE TABLE "UserLevel" (
    "userId" TEXT NOT NULL,
    "guildId" TEXT NOT NULL,
    "exp" INTEGER NOT NULL DEFAULT 0,
    "lastXp" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "blocked" BOOLEAN NOT NULL DEFAULT false,
    "blockExpires" TIMESTAMP(3),
    "colour" INTEGER,
    "background" TEXT,

    CONSTRAINT "UserLevel_pkey" PRIMARY KEY ("userId","guildId")
);

-- CreateTable
CREATE TABLE "GuildLevel" (
    "id" TEXT NOT NULL,
    "enabled" BOOLEAN NOT NULL DEFAULT true,
    "leaderboardModOnly" BOOLEAN NOT NULL DEFAULT false,
    "enableRanks" BOOLEAN NOT NULL DEFAULT true,
    "betweenXp" INTEGER NOT NULL DEFAULT 60,
    "blocklist" TEXT[],
    "rewardText" TEXT,
    "background" TEXT,
    "rewardLog" TEXT,
    "blockLog" TEXT,

    CONSTRAINT "GuildLevel_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Guild" (
    "id" TEXT NOT NULL,
    "adminRoles" TEXT[],
    "managerRoles" TEXT[],
    "moderatorRoles" TEXT[],
    "userRoles" TEXT[],

    CONSTRAINT "Guild_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CommandOverride" (
    "guildId" TEXT NOT NULL,
    "node" TEXT NOT NULL,
    "level" INTEGER NOT NULL DEFAULT 0,

    CONSTRAINT "CommandOverride_pkey" PRIMARY KEY ("guildId","node")
);

-- AddForeignKey
ALTER TABLE "UserLevel" ADD CONSTRAINT "UserLevel_guildId_fkey" FOREIGN KEY ("guildId") REFERENCES "GuildLevel"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CommandOverride" ADD CONSTRAINT "CommandOverride_guildId_fkey" FOREIGN KEY ("guildId") REFERENCES "Guild"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
