import { join, dirname } from "path";
import { fileURLToPath } from "url";

const __dirname = dirname(fileURLToPath(import.meta.url));

import fs from "fs";
import { REST } from "@discordjs/rest";
import { Routes } from "discord-api-types/v9";

import dotenv from "dotenv";
dotenv.config();

if (!process.env.TOKEN || !process.env.APP_ID) {
  console.log("TOKEN or APP_ID not set, cannot sync commands!");
  process.exit(1);
}

if (!process.env.GUILD_ID) {
  console.log(
    "No guild ID set! Sleeping for 2 seconds, abort if this is not intended."
  );

  await new Promise((resolve) => setTimeout(resolve, 2000));
}

const commands = [];

for (const file of fs
  .readdirSync(join(__dirname, "commands"))
  .filter((file) => file.endsWith(".js"))) {
  const command = (await import(`./commands/${file}`)).default;

  commands.push(command.data.toJSON());
}

const rest = new REST({ version: "9" }).setToken(process.env.TOKEN);

if (process.env.GUILD_ID) {
  console.log(`Registering commands on ${process.env.GUILD_ID}`);

  rest
    .put(
      Routes.applicationGuildCommands(process.env.APP_ID, process.env.GUILD_ID),
      { body: commands }
    )
    .then(() => console.log("Successfully registered application commands."))
    .catch(console.error);
} else {
  rest
    .put(Routes.applicationCommands(process.env.APP_ID), { body: commands })
    .then(() => console.log("Successfully registered application commands."))
    .catch(console.error);
}
