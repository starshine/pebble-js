import { Embed } from "@discordjs/builders";

import { permissionLevelFor, overrideFor } from "../lib/db.js";
import { levelToString } from "../lib/permissions.js";

export default {
  once: false,

  async execute(bot, interaction) {
    if (interaction.isCommand())
      return await this.executeCommand(bot, interaction);
    if (interaction.isAutocomplete())
      return await this.executeAutocomplete(bot, interaction);
  },

  async executeAutocomplete(bot, interaction) {
    const cmd = bot.commands.get(interaction.commandName);
    if (!cmd) return;

    try {
      if (
        interaction.options.getSubcommandGroup(false) &&
        interaction.options.getSubcommand(false)
      ) {
        await cmd[
          "autocomplete_" +
            interaction.options.getSubcommandGroup() +
            "_" +
            interaction.options.getSubcommand()
        ](bot, interaction);
      } else if (interaction.options.getSubcommand(false)) {
        await cmd["autocomplete_" + interaction.options.getSubcommand()](
          bot,
          interaction
        );
      } else {
        await cmd.autocomplete(bot, interaction);
      }
    } catch (e) {
      bot.log.error(
        `Error in autocomplete for "${interaction.commandName}":`,
        e
      );
    }
  },

  async executeCommand(bot, interaction) {
    const cmd = bot.commands.get(interaction.commandName);
    if (!cmd) return;

    if (!interaction.guild && cmd.guildOnly) {
      // yes, on the command group level
      return await interaction.reply({
        content: "This command cannot be used in DMs.",
        ephemeral: true,
      });
    }

    if (interaction.guildId) {
      const required = await overrideFor(interaction);
      const member = await permissionLevelFor(interaction.member);

      if (member < required.perms) {
        return await interaction.reply({
          embeds: [
            new Embed()
              .setTitle("Missing permissions")
              .setColor(bot.colour)
              .setDescription(
                `You're not allowed to use this command. This command needs \`${levelToString(
                  required.perms
                )}\` permissions, but you only have \`${levelToString(
                  member
                )}\`.`
              )
              .setFooter({
                text: `Effective permission node: ${required.name}`,
              }),
          ],
          ephemeral: true,
        });
      }
    }

    try {
      if (
        interaction.options.getSubcommandGroup(false) &&
        interaction.options.getSubcommand(false)
      ) {
        await cmd[
          interaction.options.getSubcommandGroup() +
            "_" +
            interaction.options.getSubcommand()
        ](bot, interaction);
      } else if (interaction.options.getSubcommand(false)) {
        await cmd[interaction.options.getSubcommand()](bot, interaction);
      } else {
        await cmd.execute(bot, interaction);
      }
    } catch (e) {
      bot.log.error(`Error in command "${interaction.commandName}":`, e);
    }
  },
};
