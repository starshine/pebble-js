export default {
  once: false,
  execute(bot) {
    bot.log.info(`Ready as ${bot.user.tag} (${bot.user.id})!`);
  },
};
