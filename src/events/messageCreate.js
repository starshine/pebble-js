import { prisma, getGuildLevel } from "../lib/db.js";

function randXp() {
  return 15 + Math.floor(Math.random() * 12);
}

export default {
  once: false,
  async execute(bot, message) {
    if (message.author.bot) return;

    if (!message.guild) return;

    const guild = await getGuildLevel(message.guild.id);

    let channelId = message.channel.id;
    if (message.channel.parent?.parentId)
      channelId = message.channel.parent.parentId;
    else if (message.channel.parentId) channelId = message.channel.parentId;

    // if channel, (parent channel if thread), or category are blocked, return
    if (guild.blocklist.includes(channelId)) return;
    // if any roles are blocked, return
    if (message.member.roles.cache.some((r) => guild.blocklist.includes(r.id)))
      return;

    const user = await prisma.userLevel.findFirst({
      where: { guildId: message.guild.id, userId: message.author.id },
    });
    // if no user, add them
    if (!user) {
      await prisma.userLevel.upsert({
        where: {
          userId_guildId: {
            userId: message.author.id,
            guildId: message.guild.id,
          },
        },
        update: {
          exp: { increment: randXp() },
        },
        create: {
          userId: message.author.id,
          guildId: message.guild.id,
          exp: randXp(),
        },
      });

      return;
    }

    // get next xp date
    const nextXp = new Date();
    nextXp.setTime(user.lastXp.getTime() + guild.betweenXp * 1000);

    // check if user is blocked
    if (user.blocked) return;
    if (nextXp > new Date()) return;

    // if no, add random xp (15-26 xp) and set last xp time to now
    await prisma.userLevel.update({
      where: {
        userId_guildId: {
          userId: message.author.id,
          guildId: message.guild.id,
        },
      },
      data: {
        exp: { increment: randXp() },
        lastXp: new Date(),
      },
    });
  },
};
