import { join, dirname } from "path";
import { fileURLToPath } from "url";
import fs from "fs";

const __dirname = dirname(fileURLToPath(import.meta.url));

import CatLoggr from "cat-loggr";
import { Client, Collection, Intents } from "discord.js";
import dotenv from "dotenv";

dotenv.config();

const bot = new Client({
  intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.DIRECT_MESSAGES,
    Intents.FLAGS.DIRECT_MESSAGE_REACTIONS,
    Intents.FLAGS.GUILD_MESSAGES,
    Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
  ],
  allowedMentions: {
    parse: ["users"],
  },
  partials: ["USER", "CHANNEL", "MESSAGE", "REACTION"],
});

bot.colour = parseInt(process.env.COLOUR || "7289da", 16);
bot.commands = new Collection();
bot.log = new CatLoggr({
  shardId: bot.shard.ids[0],
  shardLength: bot.shard.count,
});

bot.log.info("Loading events");

for (const file of fs
  .readdirSync(join(__dirname, "events"))
  .filter((file) => file.endsWith(".js"))) {
  const event = (await import(`./events/${file}`)).default;
  const name = file.slice(0, file.length - 3);

  bot.log.debug(`Loading event "${name}" (once: ${event.once})`);

  if (event.once) {
    bot.once(name, (...args) => event.execute(bot, ...args));
  } else {
    bot.on(name, (...args) => event.execute(bot, ...args));
  }

  bot.log.debug(`Loaded event "${name}"`);
}

bot.log.info("Loading commands");

for (const file of fs
  .readdirSync(join(__dirname, "commands"))
  .filter((file) => file.endsWith(".js"))) {
  const command = (await import(`./commands/${file}`)).default;
  const name = command.data.name;

  bot.log.debug(`Loading command "${name}"`);

  bot.commands.set(name, command);
}

process.on("unhandledRejection", (error) =>
  bot.log.error("Caught unhandled rejection:", error)
);
process.on("uncaughtException", (error) =>
  bot.log.error("Caught uncaught exception:", error)
);

bot.login(process.env.TOKEN);
