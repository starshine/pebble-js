import { Embed } from "@discordjs/builders";

export async function paginate(interaction, data, minutes = null) {
  if (!data.length) throw new Error("no data to paginate");
  if (data.length === 1) return await interaction.reply(data[0]);

  const msg = await interaction.reply(
    Object.assign(data[0], { fetchReply: true })
  );

  minutes = minutes ?? 15;

  let page = 0;
  const collector = msg.createReactionCollector(
    (reaction, user) =>
      ["⬅️", "➡️"].find((r) => r === reaction.emoji.name) &&
      user.id === interaction.user.id,
    { time: minutes * 60 * 1000 }
  );

  collector.on("collect", async (reaction, user) => {
    let newPage = page;

    if (reaction.emoji.name === "⬅️") {
      newPage = page - 1;
    } else {
      newPage = page + 1;
    }

    if (interaction.guild) {
      try {
        await reaction.users.remove(user.id);
        // eslint-disable-next-line no-empty
      } catch {}
    }

    if (newPage < 0) {
      newPage = data.length - 1;
    } else if (newPage > data.length - 1) {
      newPage = 0;
    }

    page = newPage;

    await msg.edit(data[newPage]);
  });

  collector.on("end", async () => {
    try {
      await msg.reactions.removeAll();
      // eslint-disable-next-line no-empty
    } catch {}
  });
}

export function stringsToEmbeds(title, colour, strings, perPage = 10) {
  const embeds = [];

  let embed = new Embed().setTitle(title).setColor(colour);

  let i = 0;
  let description = "";

  strings.forEach((string) => {
    if (i >= perPage) {
      embed.setDescription(description);
      description = "";

      embeds.push(embed);
      embed = new Embed().setTitle(title).setColor(colour);
    }
    description += string + "\n";
    i++;
  });

  if (description !== "") {
    embed.setDescription(description);
    description = "";
    embeds.push(embed);
  }

  embeds.forEach((embed, i) =>
    embed.setFooter({ text: `Page ${i + 1}/${embeds.length}` })
  );

  return embeds;
}
