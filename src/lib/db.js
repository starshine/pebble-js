import { Permissions } from "discord.js";

import pkg from "@prisma/client";
const { PrismaClient } = pkg;

import PermissionLevel, { defaultNodeLevels } from "./permissions.js";

export const prisma = new PrismaClient();

export async function getGuildLevel(id) {
  const guild = await prisma.guildLevel.findUnique({ where: { id } });
  if (guild) return guild;

  return await prisma.guildLevel.create({ data: { id } });
}

export async function overrideFor(interaction) {
  // first, check guild overrides
  const overrides = await prisma.commandOverride.findMany({
    where: { guildId: interaction.guild.id },
  });

  let perms = overrideForInner(overrides, interaction);
  if (perms) return perms;

  // if there are no guild overrides, check default perms
  return overrideForInner(defaultNodeLevels(), interaction);
}

function overrideForInner(overrideArray, interaction) {
  const overrides = {};
  overrideArray.forEach(
    (override) => (overrides[override.node] = override.level)
  );

  const root = interaction.commandName;
  const subGroup = interaction.options.getSubcommandGroup(false);
  const sub = interaction.options.getSubcommand(false);

  let name = root;

  if (subGroup && sub) name = `${root}.${subGroup}.${sub}`;
  else if (sub) name = `${root}.${sub}`;

  // if specific command has an override, return it
  let perms = overrides[name];
  if (perms != undefined) return { name, perms };

  // otherwise, get most specific wildcard
  let wildcard;
  if (subGroup && sub) wildcard = `${root}.${subGroup}.*`;
  else if (sub) wildcard = `${root}.*`;

  // if that exists, return that
  perms = overrides[wildcard];
  if (perms != undefined) return { name: wildcard, perms };

  // try least specific wildcard
  perms = overrides[`${root}.*`];
  if (perms != undefined) return { name: `${root}.*`, perms };

  return undefined;
}

export async function permissionLevelFor(member) {
  const guild = await getGuild(member.guild.id);

  // check if any of the user's roles have admin perms
  if (member.permissions.has(Permissions.FLAGS.ADMINISTRATOR))
    return PermissionLevel.ADMIN;

  // otherwise, check if any of the user's roles have bot permissions
  if (member.roles.cache.some((r) => guild.adminRoles.includes(r.id)))
    return PermissionLevel.ADMIN;
  if (member.roles.cache.some((r) => guild.managerRoles.includes(r.id)))
    return PermissionLevel.MANAGER;
  if (member.roles.cache.some((r) => guild.moderatorRoles.includes(r.id)))
    return PermissionLevel.MODERATOR;
  if (member.roles.cache.some((r) => guild.userRoles.includes(r.id)))
    return PermissionLevel.USER;

  return PermissionLevel.EVERYONE;
}

export async function getGuild(id) {
  const guild = await prisma.guild.findUnique({ where: { id } });
  if (guild) return guild;

  return await prisma.guild.create({ data: { id } });
}
