const PermissionLevel = Object.freeze({
  EVERYONE: 0,
  USER: 1,
  MODERATOR: 2,
  MANAGER: 3,
  ADMIN: 4,
});

export default PermissionLevel;

export function levelToString(level) {
  switch (level) {
    case PermissionLevel.EVERYONE:
      return "EVERYONE";
    case PermissionLevel.USER:
      return "USER";
    case PermissionLevel.MODERATOR:
      return "MODERATOR";
    case PermissionLevel.MANAGER:
      return "MANAGER";
    case PermissionLevel.ADMIN:
      return "ADMIN";
  }
  return "UNKNOWN";
}

export function stringToLevel(level) {
  switch (level.toLowerCase()) {
    case "everyone":
      return PermissionLevel.EVERYONE;
    case "user":
      return PermissionLevel.USER;
    case "moderator":
      return PermissionLevel.MODERATOR;
    case "manager":
      return PermissionLevel.MANAGER;
    case "admin":
      return PermissionLevel.ADMIN;
    default:
      return undefined;
  }
}

export const defaultNodes = {
  "level.show": PermissionLevel.USER,
  "level.leaderboard": PermissionLevel.MANAGER,
  "permissions.*": PermissionLevel.ADMIN,
};

export function defaultNodeLevels() {
  return Object.keys(defaultNodes).map((node) => {
    return { node: node, level: defaultNodes[node] };
  });
}

export const commandChoices = [
  ["Everyone", "EVERYONE"],
  ["User roles", "USER"],
  ["Moderator roles", "MODERATOR"],
  ["Manager roles", "MANAGER"],
  ["Admin roles", "ADMIN"],
];

export const nodeOptions = [
  "level.*",
  "level.show",
  "level.leaderboard",
  "permissions.*",
  "permissions.command.*",
  "permissions.command.list",
  "permissions.command.set",
  "permissions.command.remove",
];
