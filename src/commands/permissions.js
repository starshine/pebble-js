import { SlashCommandBuilder } from "@discordjs/builders";

import { prisma } from "../lib/db.js";
import {
  levelToString,
  commandChoices,
  nodeOptions,
  stringToLevel,
} from "../lib/permissions.js";
import { paginate, stringsToEmbeds } from "../lib/paginate.js";

const commandData = new SlashCommandBuilder()
  .setName("permissions")
  .setDescription("Configure Pebble's permissions!")
  .addSubcommandGroup((group) =>
    group
      .setName("command")
      .setDescription("Configure command permissions")
      .addSubcommand((sub) =>
        sub.setName("list").setDescription("List current command overrides")
      )
      .addSubcommand((sub) =>
        sub
          .setName("set")
          .setDescription("Set command overrides")
          .addStringOption((opt) =>
            opt
              .setName("node")
              .setDescription("The command node to change")
              .setRequired(true)
              .setAutocomplete(true)
          )
          .addStringOption((opt) =>
            opt
              .setName("level")
              .setDescription("Permission level to require")
              .setRequired(true)
              .setChoices(commandChoices)
          )
      )
      .addSubcommand((sub) =>
        sub
          .setName("remove")
          .setDescription("Remove command overrides")
          .addStringOption((opt) =>
            opt
              .setName("node")
              .setDescription("The command node to remove")
              .setRequired(true)
              .setAutocomplete(true)
          )
      )
  );

export default {
  data: commandData,
  guildOnly: true,

  async command_list(bot, interaction) {
    const commands = await prisma.commandOverride.findMany({
      where: { guildId: interaction.guild.id },
    });

    const overrides = commands.map(
      (node) => `\`${node.node}\`: ${levelToString(node.level)}`
    );

    overrides.sort();
    if (!overrides.length)
      return await interaction.reply(
        "There are no command overrides stored for this server."
      );

    const embeds = stringsToEmbeds(
      `Command overrides (${overrides.length})`,
      bot.colour,
      overrides
    ).map((embed) => {
      return {
        embeds: [embed],
      };
    });

    await paginate(interaction, embeds);
  },

  async autocomplete_command_remove(bot, interaction) {
    const input = interaction.options.getString("node");
    const commands = await prisma.commandOverride.findMany({
      where: { guildId: interaction.guild.id },
    });

    const nodes = commands.forEach((command) => command.node);

    if (nodes.length === 0)
      return await interaction.respond([
        { name: "No overrides set", value: "thisWillNotMatchAnything" },
      ]);

    if (!input) {
      let options = nodes
        .filter((node) => node.endsWith("*"))
        .map((node) => {
          return { name: node, value: node };
        });

      if (options.length > 25) options = options.slice(0, 24);

      return await interaction.respond(options);
    }

    const results = nodes.filter((node) =>
      node.toLowerCase().startsWith(input.toLowerCase())
    );

    if (!results.length)
      return await interaction.respond([
        { name: "No valid nodes found", value: "thisWillNotMatchAnything" },
      ]);

    let options = results.map((result) => {
      return { name: result, value: result };
    });

    if (options.length > 25) options = options.slice(0, 24);

    return await interaction.respond(options);
  },

  async autocomplete_command_set(bot, interaction) {
    const input = interaction.options.getString("node");

    if (!input) {
      // if there's no input, respond with all wildcard nodes
      let options = nodeOptions
        .filter((node) => node.endsWith("*"))
        .map((node) => {
          return { name: node, value: node };
        });

      if (options.length > 25) options = options.slice(0, 24);

      return await interaction.respond(options);
    }

    const results = nodeOptions.filter((node) =>
      node.toLowerCase().startsWith(input.toLowerCase())
    );

    if (!results.length)
      return await interaction.respond([
        { name: "No valid nodes found", value: "thisWillNotMatchAnything" },
      ]);

    let options = results.map((result) => {
      return { name: result, value: result };
    });

    if (options.length > 25) options = options.slice(0, 24);

    return await interaction.respond(options);
  },

  async command_set(bot, interaction) {
    const node = interaction.options.getString("node");

    if (!nodeOptions.some((option) => option === node))
      return await interaction.reply({
        content: `\`${node}\` is not a valid permission node.`,
        ephemeral: true,
      });

    const level = stringToLevel(interaction.options.getString("level"));
    if (level === undefined)
      return await interaction.reply({
        content: `\`${interaction.options.getString(
          "level"
        )}\` is not a valid permission level.`,
        ephemeral: true,
      });

    await prisma.commandOverride.upsert({
      where: { guildId_node: { guildId: interaction.guild.id, node: node } },
      update: { level: level },
      create: { guildId: interaction.guild.id, node: node, level: level },
    });

    return await interaction.reply(
      `Set the permission level for \`${node}\` to \`${levelToString(level)}\`!`
    );
  },

  async command_remove(bot, interaction) {
    const node = interaction.options.getString("node");
    const commands = await prisma.commandOverride.findMany({
      where: { guildId: interaction.guild.id },
    });

    if (!commands.some((s) => s.node === node))
      return await interaction.reply({
        content: `\`${node}\` is either not a valid node, or doesn't have an override set.`,
        ephemeral: true,
      });

    await prisma.commandOverride.delete({
      where: {
        guildId_node: {
          guildId: interaction.guild.id,
          node: node,
        },
      },
    });

    return await interaction.reply(`Removed override for \`${node}\`!`);
  },
};
