import { SlashCommandBuilder } from "@discordjs/builders";

export default {
  data: new SlashCommandBuilder()
    .setName("stats")
    .setDescription("Show some statistics!"),
  async execute(bot, interaction) {
    await interaction.reply("Pong!");
  },
};
