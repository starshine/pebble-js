import { SlashCommandBuilder, Embed } from "@discordjs/builders";

import { prisma, getGuildLevel } from "../lib/db.js";
import { paginate, stringsToEmbeds } from "../lib/paginate.js";

function calculateExp(level) {
  return (5.0 / 6.0) * level * (2 * Math.pow(level, 2) + 27 * level + 91);
}

function calculateLevel(xp) {
  const x = xp + 1;
  const pow = Math.cbrt(
    Math.sqrt(3) *
      Math.sqrt(3888.0 * Math.pow(x, 2) + 291600.0 * x - 207025.0) -
      108.0 * x -
      4050.0
  );

  return Math.floor(
    -pow / (2.0 * Math.pow(3.0, 2.0 / 3.0) * Math.pow(5.0, 1.0 / 3.0)) -
      (61.0 * Math.cbrt(5.0 / 3.0)) / (2.0 * pow) -
      9.0 / 2.0
  );
}

export default {
  guildOnly: true,
  data: new SlashCommandBuilder()
    .setName("level")
    .setDescription("Get level information!")
    .addSubcommand((sub) => {
      return sub
        .setName("show")
        .setDescription("Show you, or another user's, level!")
        .addUserOption((option) =>
          option
            .setName("user")
            .setDescription("The user to show")
            .setRequired(false)
        );
    })
    .addSubcommand((sub) => {
      return sub
        .setName("leaderboard")
        .setDescription("Show this server's leaderboard!");
    }),

  async show(bot, interaction) {
    const config = await getGuildLevel(interaction.guild.id);
    if (!config.enabled)
      return await interaction.reply({
        content: "Levels aren't enabled on this server.",
        ephemeral: true,
      });

    const user = interaction.options.getUser("user") ?? interaction.user;
    let rank = 0;

    const level = await prisma.userLevel.findFirst({
      where: { guildId: interaction.guild.id, userId: user.id },
    });

    if (!level)
      return await interaction.reply({
        content: `${user.tag} doesn't have a level card.`,
        ephemeral: true,
      });

    if (config.enableRanks) {
      const leaderboard = await prisma.userLevel.findMany({
        where: { guildId: interaction.guild.id },
        orderBy: { exp: "desc" },
      });

      const userRank = leaderboard.findIndex(
        (level) => level.userId === user.id
      );
      if (userRank !== -1) rank = userRank + 1;
    }

    let colour = level.colour;
    let name = user.tag;
    let avatarURL = user.avatarURL();
    try {
      const member = await interaction.guild.members.fetch(user.id);

      if (!colour) colour = member.displayColor;
      if (member.nickname) name = member.nickname;
      if (member.avatar) avatarURL = member.avatarURL();
    } catch (e) {
      bot.log.error("error fetching member", e);
    }

    const lvl = calculateLevel(level.exp);
    const xpForNext = calculateExp(lvl + 1);
    const xpForPrev = calculateExp(lvl);

    const progress = level.exp - xpForPrev;
    const needed = xpForNext - xpForPrev;

    const embed = new Embed()
      .setTitle(`Level ${calculateLevel(level.exp)}`)
      .setAuthor({ name: name })
      .setThumbnail(avatarURL)
      .setColor(colour)
      .setDescription(`${level.exp}/${xpForNext} XP`)
      .addField({
        name: "Progress to next level",
        value: `${Math.round(
          (progress / needed) * 100
        )}% (${progress}/${needed})`,
      })
      .setTimestamp();
    if (rank != 0) embed.title += ` - Rank #${rank}`;

    await interaction.reply({ embeds: [embed] });
  },

  async leaderboard(bot, interaction) {
    const config = await getGuildLevel(interaction.guild.id);

    if (!config.enabled || !config.enableRanks)
      return await interaction.reply({
        content: "The leaderboard isn't enabled on this server.",
        ephemeral: true,
      });

    const leaderboard = await prisma.userLevel.findMany({
      where: { guildId: interaction.guild.id },
      orderBy: { exp: "desc" },
    });

    if (!leaderboard.length)
      return await interaction.reply({
        content: "There are no users on the leaderboard.",
        ephemeral: true,
      });

    const strings = leaderboard.map(
      (user, i) =>
        `${i + 1}. <@!${user.userId}> \`${
          user.exp
        }\` XP, level \`${calculateLevel(user.exp)}\``
    );

    const embeds = stringsToEmbeds(
      `Leaderboard for ${interaction.guild.name}`,
      bot.colour,
      strings,
      20
    ).map((embed) => {
      return { embeds: [embed] };
    });

    await paginate(interaction, embeds);
  },
};
