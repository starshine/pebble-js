import { join, dirname } from "path";
import { fileURLToPath } from "url";

const __dirname = dirname(fileURLToPath(import.meta.url));

import CatLoggr from "cat-loggr";
import { ShardingManager } from "discord.js";
import dotenv from "dotenv";

const log = new CatLoggr();

dotenv.config();

// sync commands (if not disabled)
if (process.env.SYNC_COMMANDS != "false") {
  await import(join(__dirname, "sync-commands.js"));
}

const manager = new ShardingManager(join(__dirname, "bot.js"), {
  token: process.env.TOKEN,
});

manager.on("shardCreate", (shard) => log.info(`Launched shard ${shard.id}`));

manager.spawn();
